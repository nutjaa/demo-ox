import { useState } from "react";
import Head from "next/head";
import { Row, Col, Modal } from "antd";
import { CloseOutlined, CloudOutlined } from "@ant-design/icons";
import { checkGameResult, aiNextTurn } from "../services/ox.service";

export default function Home() {
  const [values, setValues] = useState(Array(9).fill(null));
  const [gameState, setGameState] = useState("gameon");

  const resetGame = () => {
    setValues(Array(9).fill(null));
    setGameState("gameon");
  };

  interface OxCellProps {
    value?: Boolean;
    callback?: Function;
  }
  const OxCell = ({ value, callback }: OxCellProps) => {
    let icon = <></>;
    if (value == true) {
      icon = <CloudOutlined style={{ fontSize: "3em" }} />;
    } else if (value == false) {
      icon = <CloseOutlined style={{ fontSize: "3em" }} />;
    }
    return (
      <Col
        span={8}
        style={{ backgroundColor: "black" }}
        onClick={() => callback()}
      >
        <Row
          style={{ height: "20vh", backgroundColor: "white" }}
          justify="center"
          align="middle"
        >
          <Col>{icon}</Col>
        </Row>
      </Col>
    );
  };

  const UserClickCell = (index: number) => {
    if (values[index] != null) {
      console.log("Already selected");
      return;
    }
    values[index] = true;
    setValues([...values]);
    // Are u win?
    if (processGameResult(checkGameResult(values))) {
      return;
    }
    // Ai turn
    values[aiNextTurn(values)] = false;
    setValues([...values]);
    // An ai win?
    if (processGameResult(checkGameResult(values))) {
      return;
    }
    // wait for next turn
  };

  const processGameResult = (result: string): boolean => {
    if (result == "win") {
      Modal.success({
        content: "You win",
        onOk: resetGame,
      });
      return true;
    }
    if (result == "loose") {
      Modal.error({
        content: "You loose",
        onOk: resetGame,
      });
      return true;
    }
    if (result == "draw") {
      Modal.info({
        content: "Draw",
        onOk: resetGame,
      });
      return true;
    }

    return false;
  };

  return (
    <>
      <Head>
        <title>OX - Demo</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Row
        justify="center"
        align="middle"
        style={{
          padding: 20,
          minHeight: "100vh",
          textAlign: "center",
          backgroundColor: "black",
        }}
      >
        <Col style={{ maxWidth: 1170, width: "100%" }}>
          <Row gutter={[2, 2]}>
            {values.map((value, index) => {
              return (
                <OxCell
                  key={`cell-${index}`}
                  value={value}
                  callback={() => UserClickCell(index)}
                />
              );
            })}
          </Row>
        </Col>
      </Row>
    </>
  );
}
