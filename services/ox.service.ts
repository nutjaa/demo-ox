export const checkGameResult = (values: any[]): string => {
  const winConditions = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];

  const userSelected = values
    .map((e, i) => (e == true ? i : undefined))
    .filter((x) => x != undefined);

  let result;

  winConditions.map((winCondition) => {
    const found = winCondition.every((r) => userSelected.includes(r));
    if (found) {
      result = "win";
    }
  });

  if (result) {
    return result;
  }

  const aiSelected = values
    .map((e, i) => (e == false ? i : undefined))
    .filter((x) => x != undefined);

  winConditions.map((winCondition) => {
    const found = winCondition.every((r) => aiSelected.includes(r));
    if (found) {
      result = "loose";
    }
  });

  if (result) {
    return result;
  }

  const notSelect = values
    .map((e, i) => (e == null ? i : undefined))
    .filter((x) => x != undefined);
  console.log(notSelect.length);
  if (notSelect.length == 0) {
    result = "draw";
  }

  return result;
};

// Stupid bot
export const aiNextTurn = (values: any[]): number => {
  const notSelected = values
    .map((e, i) => (e == null ? i : undefined))
    .filter((x) => x != undefined);
  // Check for win
  for (let i = 0; i < notSelected.length; i++) {
    let newValues = [...values];
    newValues[notSelected[i]] = false;
    if (checkGameResult(newValues) == "loose") {
      return notSelected[i];
    }
  }

  // Check for defend
  for (let i = 0; i < notSelected.length; i++) {
    let newValues = [...values];
    newValues[notSelected[i]] = true;
    if (checkGameResult(newValues) == "win") {
      return notSelected[i];
    }
  }

  // Check for middle
  if (values[4] == null) {
    return 4;
  }

  // Check for corner
  let corners = [0, 2, 6, 8].filter((corner) => values[corner] == null);
  if (corners.length > 0) {
    return corners[Math.floor(Math.random() * corners.length)];
  }

  // Rendom rest
  return notSelected[Math.floor(Math.random() * notSelected.length)];
};
